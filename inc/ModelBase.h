#ifndef MODELBASE_H
#define MODELBASE_H

#include <QAbstractItemModel>
#include <FrontendCommon.h>

class FRONTENDSHARED_EXPORT ModelBase : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum ModelBaseRoles {
        qmlFileRole = Qt::UserRole + 1,
        modelRole,
        dataRole
    };
    explicit ModelBase(QObject* parent = nullptr);

    void addChild(ModelBase* child);
    void deleteChild(int index);
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex index(int row, int column = 0,
                      const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    virtual QVariant getDataToObjectRole(ModelBase* modelBase) const;
    int columnCount(const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;

    QString getQmlFile() const;
    void setQmlFile(const QString& getQmlFile);

signals:

public slots:
    //void childrenInserted(const QModelIndex &parent, int start, int end);

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QString _qmlFile;
};

#endif // MODELBASE_H
