#ifndef VIEWCREATOR_H
#define VIEWCREATOR_H

#include <QObject>
#include <QQmlEngine>
#include <QQmlComponent>
//#include <QMap>
#include <QUuid>

#include "FrontendCommon.h"

namespace Planar{
namespace Frontend{

class FRONTENDSHARED_EXPORT BaseDelegateCreator
{

public:
    BaseDelegateCreator(QString key, QUuid uuid);

    typedef QMap<QUuid, BaseDelegateCreator*> ViewFactory;

    static BaseDelegateCreator::ViewFactory getViewFactory(QString key);

    static QObject* createDelegate(QString key, QUuid uuid);

    static QObject* createDelegate(ViewFactory* factory, QUuid uuid);

protected:
    virtual QObject* create() = 0;

private:
    static QMap<QString, ViewFactory> ViewFactories;
};

class FRONTENDSHARED_EXPORT ObjectDelegateCreator : public BaseDelegateCreator
{
public:
    ObjectDelegateCreator(QString key, QUuid uuid, QMetaObject viewObject);
    virtual QObject* create() override;

private:
    QMetaObject _viewObject;
};

class FRONTENDSHARED_EXPORT ComponentDelegateCreator: public BaseDelegateCreator
{
public:
    ComponentDelegateCreator(QString key, QUuid uuid, QQmlEngine* engine, QString fileName);
    ~ComponentDelegateCreator();
    virtual QObject* create() override;

private:
    QQmlComponent* _component;
    QString _fileName;
    QQmlEngine* _engine;
};


}
}



#endif // VIEWCREATOR_H
