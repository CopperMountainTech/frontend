#ifndef UNITHELPER_H
#define UNITHELPER_H

#include "FrontendCommon.h"
#include <QObject>
#include <QAbstractListModel>
#include <Units.h>


namespace Planar {
namespace Frontend {

//TODO: добавить в неймспейс классы UnitProperty и UnitTypeListModel
class FRONTENDSHARED_EXPORT UnitTypeListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int currentIndex READ currentIndex NOTIFY currentIndexChanged)
public:
    enum UnitTypeListRoles {
        NameRole = Qt::UserRole + 1,
        MultiplierRole
    };

    UnitTypeListModel(Planar::Unit* unit, QObject* parent = 0);

    int rowCount(const QModelIndex& parent) const;
    QModelIndex index(int row, int column = 0,
                      const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const;
    QHash<int, QByteArray> roleNames() const;
    void update();
    Q_INVOKABLE double multiplier(int index);
    int currentIndex() const;

private:
    void loadNormalizationTable();
    void updateCurrentIndex(bool forceUpdateIndex);

signals:
    void currentIndexChanged(int currentIndex);

private:
    Planar::Unit* _unit;
    typedef std::pair<QString, double> UnitTypePair;
    QList<UnitTypePair> _listTypes;
    int _currentIndex;
    Planar::PartialUnit** _normTable;
};


class FRONTENDSHARED_EXPORT UnitHelper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Planar::Unit unitValue READ unitValue WRITE changeUnitValue NOTIFY
               unitValueChanged)
    Q_PROPERTY(double valuePart READ valuePart WRITE changeValuePart NOTIFY valuePartChanged)
    Q_PROPERTY(double multiplierPart READ multiplierPart WRITE changeMultiplierPart NOTIFY
               multiplierPartChanged)
    Q_PROPERTY(UnitTypeListModel* unitTypeList READ unitTypeList NOTIFY unitTypeListChanged)
public:
    explicit UnitHelper(QObject* parent = nullptr);

    Planar::Unit unitValue() const;
    double valuePart() const;
    double multiplierPart() const;
    UnitTypeListModel* unitTypeList();

signals:
    void unitValueChanged(Planar::Unit unitValue);
    void valuePartChanged(double valuePart);
    void multiplierPartChanged(double multiplierPart);
    void unitTypeListChanged(UnitTypeListModel* unitTypeList);

public slots:
    void changeUnitValue(Planar::Unit value);
    void changeValuePart(double valuePart);
    void changeMultiplierPart(double multiplierPart);

private:
    Planar::Unit _value;
    UnitTypeListModel _unitTypeList;
};

}
}

#endif // UNITHELPER_H
