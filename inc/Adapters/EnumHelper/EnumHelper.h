#ifndef ENUMHELPER_H
#define ENUMHELPER_H

#include "FrontendCommon.h"
#include <QMetaObject>
#include <QMetaEnum>
#include <QtCore/QMutex>


namespace Planar {
namespace Frontend {

class FRONTENDSHARED_EXPORT EnumHelper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant enumValue READ enumValue WRITE setEnumValue NOTIFY enumValueChanged)
    Q_PROPERTY(int indexEnum READ indexEnum WRITE setIndexEnum NOTIFY indexEnumChanged)
    Q_PROPERTY(QStringList enumList READ enumList NOTIFY enumListChanged)

public:
    explicit EnumHelper(QObject* parent = nullptr);

    QVariant enumValue() const;
    int indexEnum() const;
    QStringList enumList() const;

public slots:
    void setEnumValue(QVariant enumValue);
    void setIndexEnum(int indexEnum);

signals:
    void enumValueChanged();
    void indexEnumChanged(int indexEnum);
    void enumListChanged(QStringList enumList);

protected:
    void parseValue();
    void updateIndex();

protected:
    QVariant _enumValue;
    int _indexEnum;
    QStringList _enumList;
    QList<int> _listValue;
    int _enumTypeId;
};

}
}

#endif // ENUMHELPER_H
