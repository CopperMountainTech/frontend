#ifndef BOXMODEL_H
#define BOXMODEL_H

#include <AimTreeNode.h>
#include <QColor>


namespace Planar {
namespace Frontend {

class FRONTENDSHARED_EXPORT BoxModel : public AimTreeNode
{
    Q_OBJECT
    Q_PROPERTY(QColor color READ getColor WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(qreal width READ getWidth WRITE setWidth NOTIFY widthChanged)
    Q_PROPERTY(qreal height READ getHeight WRITE setHeight NOTIFY heightChanged)

public:
    explicit BoxModel(QObject* parent = nullptr);
    QColor getColor() const;
    qreal getWidth() const;
    qreal getHeight() const;

public slots:
    void setColor(QColor color);
    void setWidth(qreal width);
    void setHeight(qreal height);


signals:
    void colorChanged(QColor color);
    void widthChanged(qreal width);
    void heightChanged(qreal height);

private:
    QColor _color;
    qreal _width;
    qreal _height;
};

}
}

#endif // BOXMODEL_H
