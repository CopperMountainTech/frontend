#ifndef BUTTONMODEL_H
#define BUTTONMODEL_H

#include <AimTreeNode.h>
#include "QQmlApplicationEngine"
#include "QQmlComponent"


namespace Planar {
namespace Frontend {

class FRONTENDSHARED_EXPORT ButtonModel : public AimTreeNode
{
    Q_OBJECT
    Q_PROPERTY(QString buttonName READ getButtonName WRITE setButtonName NOTIFY buttonNameChanged)
    Q_PROPERTY(int buttonState READ getButtonState WRITE setButtonState NOTIFY buttonStateChanged)
public:

    explicit ButtonModel(QObject* parent = nullptr);

    QString getButtonName() const;
    int getButtonState() const;

public slots:
    void setButtonName(QString buttonName);
    void setButtonState(int buttonState);

signals:
    void buttonNameChanged(QString buttonName);
    void buttonStateChanged(int buttonState);


private:
    QString _nameButton;
    int _stateButton;
};

}
}

#endif // BUTTONMODEL_H
