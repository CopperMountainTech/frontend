#ifndef VALUEBOX_H
#define VALUEBOX_H

#include <Model/Box/BoxModel.h>


namespace Planar {
namespace Frontend {

class FRONTENDSHARED_EXPORT ValueBoxModel : public BoxModel
{
    Q_OBJECT
    Q_PROPERTY(QString description READ getDescription WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(QString value READ getValue WRITE setValue NOTIFY valueChanged)

public:
    ValueBoxModel(QObject* parent = nullptr);
    QString getDescription() const;
    QString getValue() const;

public slots:
    void setDescription(QString getDescription);
    void setValue(QString getValue);

signals:
    void descriptionChanged(QString getDescription);
    void valueChanged(QString getValue);

private:
    QString _description;
    QString _value;
};

}
}

#endif // VALUEBOX_H
