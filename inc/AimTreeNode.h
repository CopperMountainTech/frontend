#ifndef MODELBASE_H
#define MODELBASE_H

#include <QAbstractItemModel>
#include "FrontendCommon.h"
#include "DelegateCreator.h"

namespace Planar {
namespace Frontend {

class FRONTENDSHARED_EXPORT AimTreeNode : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum AimTreeNodeRoles {
        delegateRole = Qt::UserRole + 1,
        modelRole,
        dataRole
    };
    explicit AimTreeNode(QObject* parent = nullptr);
    explicit AimTreeNode(QString viewFactoryKey, QUuid uuid, QObject* parent = nullptr);

    void addChild(AimTreeNode* child);
    void deleteChild(int index);
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex index(int row, int column = 0,
                      const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

    virtual QVariant getDataToObjectRole() const;
    int columnCount(const QModelIndex& parent) const override;
    QModelIndex parent(const QModelIndex& child) const override;

    //QString getQmlFile() const;
    //void setQmlFile(const QString& getQmlFile);



signals:

public slots:
    //void childrenInserted(const QModelIndex &parent, int start, int end);

protected:
    bool isDelegateExist(QUuid uuid) {
        return BaseDelegateCreator::getViewFactory(_viewFactoryKey).contains(uuid);
    }
    QHash<int, QByteArray> roleNames() const override;
    QString _viewFactoryKey;


private:
    QUuid _uuid;
    //QString _qmlFile;
    QVariant getDelegate() const;
};

}
}

#endif // MODELBASE_H
