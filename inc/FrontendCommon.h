#pragma once

#include <QtCore/qglobal.h>

#if defined(FRONTEND_LIBRARY)
#  define FRONTENDSHARED_EXPORT Q_DECL_EXPORT
#else
#  define FRONTENDSHARED_EXPORT Q_DECL_IMPORT
#endif

#include <QString>
#include <QSettings>
#include <QObject>
#include <QDebug>
