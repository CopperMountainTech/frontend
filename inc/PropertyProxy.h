#ifndef PROPERTYPROXY_H
#define PROPERTYPROXY_H

#include <QAbstractListModel>
#include <qmetaobject.h>
#include <FrontendCommon.h>
#include <PropertyBase.h>


namespace Planar {
namespace Frontend {

class FRONTENDSHARED_EXPORT  InvokedObject : public QObject
{
public:
    explicit InvokedObject(QObject* targetObject, const QMetaMethod& method, QObject* parent = nullptr);
    void invoke();
private:
    QObject* _targetObject;
    QMetaMethod _method;
};

class FRONTENDSHARED_EXPORT PropertyProxy : public QAbstractListModel
{
    Q_OBJECT
public:
    enum PropertyProxyRoles {
        propertyObjectRole = Qt::UserRole + 1
    };

    explicit PropertyProxy(QObject* propertiesOwner, QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QModelIndex index(int row, int column = 0,
                      const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    QObject* _propertiesOwner;
    QList<QObject*> _propertyItemList;
};

}
}

#endif // PROPERTYPROXY_H
